package com.kosign.democamerax;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Rational;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.google.common.util.concurrent.ListenableFuture;
import com.kosign.comm.Constant;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static androidx.camera.core.ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY;

public class CameraXActivity extends AppCompatActivity implements View.OnClickListener {

    private Executor executor = Executors.newSingleThreadExecutor();
    private int REQUEST_CODE_PERMISSIONS = 1001;
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    PreviewView cameraView;
    Preview mPreview;
    ImageCapture mImageCapture;
    ImageCapture.Builder mBuilder;
    ImageAnalysis mImageAnalysis;
    CameraSelector mCameraSelector;
    int lensFacing;
    boolean cameraMode = true;
    Uri uri = null;
    private ImageView captureButton,turnButton;
    private View shutterEffect, fl_camera, fl_photo;
    private TouchImageView photo;
    static Bitmap bitmap;
    String filePath;
    private File file;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camerax);

        fl_camera = findViewById(R.id.fl_camera);
        cameraView = findViewById(R.id.camera_view);
        shutterEffect = findViewById(R.id.shutter_effect);
        captureButton = findViewById(R.id.shutter);
        turnButton = findViewById(R.id.turn);
        fl_photo = findViewById(R.id.fl_photo);
        photo = findViewById(R.id.photo_view);
        View button_cancel = findViewById(R.id.button_cancel);
        View iv_rotate = findViewById(R.id.iv_rotate);
        View button_ok = findViewById(R.id.button_ok);

        button_cancel.setOnClickListener(this);
        iv_rotate.setOnClickListener(this);
        button_ok.setOnClickListener(this);
        turnButton.setOnClickListener(this);

        if(allPermissionsGranted()){
            startCamera(); //start camera if permission has been granted by user
        } else{
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }

    }

    private boolean allPermissionsGranted(){

        for(String permission : REQUIRED_PERMISSIONS){
            if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE_PERMISSIONS){
            if(allPermissionsGranted()){
                startCamera();
            } else{
                this.finish();
            }
        }
    }

    /*
    * Start Using Camera
    * */
    private void startCamera() {
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {
                    //ProcessCameraProvider cameraProvider = (ProcessCameraProvider) cameraProviderFuture.get();
                    bindPreview();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }

    @SuppressLint({"RestrictedApi", "WrongConstant"})
    void bindPreview() {
        CameraX.unbindAll();
         mPreview = new Preview.Builder()
                .build();

         if (lensFacing == 1){
             mCameraSelector = new CameraSelector.Builder()
                     .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
                     .build();

         }else if (lensFacing == 0){
             mCameraSelector = new CameraSelector.Builder()
                     .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                     .build();
         }


         /*Use for capture image*/
         mImageAnalysis = new ImageAnalysis.Builder()
                .setBackgroundExecutor(executor)
                .build();

         mBuilder = new ImageCapture.Builder();

         mImageCapture = mBuilder
                .setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
                .setCameraSelector(mCameraSelector)
                 .setIoExecutor(executor)
                 .setTargetAspectRatioCustom(new Rational(1, 1))
                .setCaptureMode(CAPTURE_MODE_MINIMIZE_LATENCY)
                .build();


         /*show camera preview*/
        mPreview.setSurfaceProvider(cameraView.createSurfaceProvider());

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playShutterAnimation();
                ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(createImageFile()).build();

                mImageCapture.takePicture(outputFileOptions, executor, new ImageCapture.OnImageSavedCallback () {
                    @Override
                    public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                   // getBitmapFormUri(CameraXActivity.this,uri);
                                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                                    Matrix matrix = new Matrix();
                                    matrix.postRotate(90);
                                    if (lensFacing == 1){
                                        matrix.postRotate(180);
                                    }
                                    bitmap =  Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                                    fl_camera.setVisibility(View.GONE);
                                    fl_photo.setVisibility(View.VISIBLE);
                                    photo.setImageBitmap(bitmap);
                                    photo.setScaleY(1f);
                                    photo.setScaleX(1f);
                                    photo.resetZoom();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    @Override
                    public void onError(@NonNull ImageCaptureException error) {
                        Log.d(">>>", "Error:>> Unsuccessful ! ");
                        error.printStackTrace();
                    }
                });
            }
        });
        CameraX.bindToLifecycle(this, mCameraSelector, mPreview, mImageAnalysis, mImageCapture);
    }

    /**
     * Effect while take photos
     * */
    private void playShutterAnimation() {
        shutterEffect.setVisibility(View.VISIBLE);
        shutterEffect.animate().alpha(0f).setDuration(300).setListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        shutterEffect.setVisibility(View.GONE);
                        shutterEffect.setAlpha(0.8f);
                    }
                });
    }
    /*
    * create file image save in device directory 
    * */
    public File createImageFile() {

        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        Constant.Photo.TAKE_PHOTO_FILE_NAME = mDateFormat.format(currentTime) + ".jpg";

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/DCIM/DemoCameraX");
        if (!myDir.exists()){
            myDir.mkdirs();
        }
        filePath = "/DCIM/DemoCameraX/" + Constant.Photo.TAKE_PHOTO_FILE_NAME;
        File file = new File(root, filePath);
        try {
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            uri = Uri.fromFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    private String bitmapToFile(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapData = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            try {
                if (fos != null)
                    fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.d(">>>", "bitmapToFile: "+file.getAbsoluteFile());
        return file.getAbsolutePath();
    }

    /*
    * Show camera after capture while click btn_cancel
    * */
    private void showCamera(){
        fl_camera.setVisibility(View.VISIBLE);
        fl_photo.setVisibility(View.GONE);
        startCamera();
    }

    /**
     * Get pictures through uri and compress them
     *
     * @param uri
     */
    public  Bitmap getBitmapFormUri(Activity ac, Uri uri) throws FileNotFoundException, IOException {
        InputStream input = ac.getContentResolver().openInputStream(uri);
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        int originalWidth = onlyBoundsOptions.outWidth;
        int originalHeight = onlyBoundsOptions.outHeight;
        if ((originalWidth == -1) || (originalHeight == -1))
            return null;
        //Image resolution is based on 480x800
        float hh = cameraView.getHeight();//The height is set as 800f here
        float ww = cameraView.getWidth();//Set the width here to 480f
        //Zoom ratio. Because it is a fixed scale, only one data of height or width is used for calculation
        int be = 1;//be=1 means no scaling
        if (originalWidth > originalHeight && originalWidth > ww) {//If the width is large, scale according to the fixed size of the width
            be = (int) (originalWidth / ww);
        } else if (originalWidth < originalHeight && originalHeight > hh) {//If the height is high, scale according to the fixed size of the width
            be = (int) (originalHeight / hh);
        }
        if (be <= 0)
            be = 1;
        //Proportional compression
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = be;//Set scaling
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        input = ac.getContentResolver().openInputStream(uri);
        bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();

        return compressImage(bitmap);//Mass compression again
    }

    /**
     * Mass compression method
     *
     * @param image
     * @return
     */
    public  Bitmap compressImage(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//Quality compression method, here 100 means no compression, store the compressed data in the BIOS
        int options = 100;
        while (baos.toByteArray().length / 1024 > 100) {  //Cycle to determine if the compressed image is greater than 100kb, greater than continue compression
            baos.reset();//Reset the BIOS to clear it
            //First parameter: picture format, second parameter: picture quality, 100 is the highest, 0 is the worst, third parameter: save the compressed data stream
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//Here, the compression options are used to store the compressed data in the BIOS
            options -= 10;//10 less each time
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//Store the compressed data in ByteArrayInputStream
        bitmap = BitmapFactory.decodeStream(isBm, null, null);//Generate image from ByteArrayInputStream data

        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        if (lensFacing == 1){
            matrix.postRotate(180);
        }
        bitmap =  Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        Log.d(">>>", "compressImage: "+bitmap);
        fl_camera.setVisibility(View.GONE);
        fl_photo.setVisibility(View.VISIBLE);
        photo.setImageBitmap(bitmap);
        photo.setScaleY(1f);
        photo.setScaleX(1f);
        photo.resetZoom();
        return bitmap;
    }


    private boolean safe=true;
    private void rotateImage(){
        /* start preview */
        int h = photo.getWidth(); // get width of screen
        int w = photo.getHeight(); // get height
        if(photo.getRotation()/90%2==0){
            h=photo.getHeight();
            w=photo.getWidth();
        }
        if(safe){
            photo.animate().rotationBy(90)
                    .scaleX(photo.getRotation()/90%2==0?(w*1.0f/h):1)
                    .scaleY(photo.getRotation()/90%2==0?(w*1.0f/h):1)
                    .setDuration(300)
                    .setInterpolator(new LinearInterpolator())
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            safe=false;
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            safe=true;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) { }

                        @Override
                        public void onAnimationRepeat(Animator animation) { }
                    }).start();
        }
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()){
                case R.id.turn:
                    if (cameraMode){
                        cameraMode = false;
                        lensFacing = 1;
                        startCamera();
                    }else {
                        cameraMode = true;
                        lensFacing = 0;
                        startCamera();
                    }
                    break;
                case R.id.button_cancel:
                    showCamera();
                    break;

                case R.id.iv_rotate:
                    rotateImage();
                    break;

                case R.id.button_ok:
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    bitmap =  Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    Constant.Photo.TAKE_PHOTO_BITMAP = bitmap;
                    Log.d(">>>", "onClick: "+bitmap);
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}