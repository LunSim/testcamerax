package com.kosign.comm;

import android.graphics.Bitmap;

public class Constant {

    public static class Photo {
        public static Bitmap TAKE_PHOTO_BITMAP;
        public static String TAKE_PHOTO_FILE_NAME;
    }
}
